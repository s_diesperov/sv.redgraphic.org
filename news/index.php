<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новости");
?>

<div class="wrapper">
<?if(count($_REQUEST["ELEMENT_CODE"]) == 0):?>

	<?if(CModule::IncludeModule("iblock"))
	{ 
    	$newsCounter = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 4, "ACTIVE" => "Y"), Array());
	}?>

    	<div class="column">
            <h1 class="page-title">Новости</h1>
            <div class="news-box _1" id="news-box" data-count="<?=$newsCounter?>">
				<?$APPLICATION->IncludeFile("/include/ajax_news.php");?>
                <div class="loader"></div>
            </div><!-- [ news-box ] -->
        </div><!-- [ column ] -->

<?else:?>

	<?$APPLICATION->IncludeComponent("bitrix:news.detail", "news_detail", array(
	"IBLOCK_TYPE" => "news",
	"IBLOCK_ID" => "4",
	"ELEMENT_ID" => "",
	"ELEMENT_CODE" => $_REQUEST["ELEMENT_CODE"],
	"CHECK_DATES" => "Y",
	"FIELD_CODE" => array(
		0 => "NAME",
		1 => "PREVIEW_TEXT",
		2 => "PREVIEW_PICTURE",
		3 => "DETAIL_TEXT",
		4 => "DATE_ACTIVE_FROM",
		5 => "",
	),
	"PROPERTY_CODE" => array(
		0 => "SLIDER",
		1 => "",
	),
	"IBLOCK_URL" => "/news/",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "A",
	"CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y",
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"SET_STATUS_404" => "N",
	"SET_TITLE" => "Y",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
	"ADD_SECTIONS_CHAIN" => "Y",
	"ADD_ELEMENT_CHAIN" => "N",
	"ACTIVE_DATE_FORMAT" => "",
	"USE_PERMISSIONS" => "N",
	"PAGER_TEMPLATE" => "",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Страница",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "N",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"USE_SHARE" => "N",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);?>

<?endif;?>

</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>