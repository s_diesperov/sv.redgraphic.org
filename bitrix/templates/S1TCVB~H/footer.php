   

        </div><!-- [ content ] -->
        <!-- [> CONTENT <] -->


        <?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
            die();?>

	       <!-- [> FOOTER <] -->
            <footer class="main">
            	<div class="wrap">
                    <div class="copyright"><?=$dbPr["FOOTER_COPYRIGHT"]["VALUE"]?></div>
                    <nav class="social">
                        <a class="fb" href="<?=$dbPr["FOOTER_SOCIAL_FB"]["VALUE"]?>"></a>
                        <a class="in" href="<?=$dbPr["FOOTER_SOCIAL_IN"]["VALUE"]?>"></a>
                        <a class="yt" href="<?=$dbPr["FOOTER_SOCIAL_YT"]["VALUE"]?>"></a>
                    </nav><!-- [ social ] -->
                    <div class="developer"><a href="#">Разработка сайта Red Graphic Interactive Agency</a></div>
                </div><!-- [ wrap ] -->
            </footer><!-- [ main ] -->
            <!-- [> FOOTER <] -->
            
        </div><!-- [ page ] -->
        <!-- [> PAGE <] -->
        
        <!-- [> HIDDEN <] -->
        <div class="hidden">
        </div><!-- [ hidden ] -->
        <!-- [> HIDDEN <] -->
        
    </body>
</html>