<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<?if(!empty($arResult["ERROR_MESSAGE"]))
{
	foreach($arResult["ERROR_MESSAGE"] as $v)
		ShowError($v);
}
if(strlen($arResult["OK_MESSAGE"]) > 0)
{
	?><div class="mf-ok-text"><?=$arResult["OK_MESSAGE"]?></div><?
}
?>

<div class="form-section bottom _1">
	<div class="display message">
    	<div class="done">
            <div class="wrap">
                <div class="title">Спасибо за вашу заявку.</div>
                <p>В течении суток с Вами свяжется наш менеджер.</p>
            </div><!-- [ wrap ] -->
        </div><!-- [ done ] -->
    	<div class="error">
            <div class="wrap">
                <div class="title">Сообщение не было отправлено</div>
                <p>Попробуйте повторить попытку отправки формы еще раз.</p>
            </div><!-- [ wrap ] -->
        </div><!-- [ error ] -->
        <button class="close" type="button">&times;</button>
    </div><!-- [ message ] -->
	<div class="display begin">
        <div class="wrap">
            <h2>свяжитесь с нами</h2>
            <div class="contact-list _1">
                <a href="tel:+375177869910">+375 17 786-99-10</a>
                <a href="mailto:info@sv-machinen.by">info@sv-machinen.by</a>
            </div><!-- [ contact-list ] -->
        	<button class="_1" type="button">заказать звонок</button>
        </div><!-- [ wrap ] -->
    </div><!-- [ begin ] -->
	

	<div class="display form">
        <form id="order-call-form" action="<?=POST_FORM_ACTION_URI?>" method="POST">
		<?=bitrix_sessid_post()?>
            <div class="wrap">
        		<div class="title">Заказать звонок</div>
                <div class="row _1">
                	<div class="cell">
                    	<label>Ваше имя или название кампании:</label>
                        <input class="_letters" name="user_name" type="text" value="" required>
                    </div><!-- [ cell ] -->
                	<div class="cell">
                    	<label>Номер телефона:</label>
                        <input class="_phone" name="user_phone" type="text" value="" required>
                    </div><!-- [ cell ] -->
                </div><!-- [ row ] -->
                <button class="_1 disabled" type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>"><?=GetMessage("MFT_SUBMIT")?></button>
            </div><!-- [ wrap ] -->
            <button class="close" type="button">&times;</button>
        </form><!-- [ order-call-form ] -->
    </div><!-- [ form ] -->
</div><!-- [ form-section ] -->