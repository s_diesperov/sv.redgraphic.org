<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<pre style="display:none;"><?print_r($arResult["PROPERTIES"]["FILES"])?></pre>

<div class="wrapper">
	<?
		$rs = $arResult['NAME'];
		foreach ($arResult['PROPERTIES']["MODELS"]["VALUE"] as $models):
			$rs .= ", ".$models;
		endforeach;
	?>
	<??>
    <h1 class="page-title"><?=$rs?></h1>
    

    <?if($arResult["PROPERTIES"]["SLIDER"]["VALUE"]):?>
        <div class="photo-slider">
            <div class="slides">
                <?foreach($arResult["PROPERTIES"]["SLIDER"]["VALUE"] as $sl => $value):?>
                    <div class="item"><img src="<?=CFile::GetPath($value)?>" alt="<?=$arResult["PROPERTIES"]["SLIDER"][$sl]["DESCRIPTION"]?>"></div>
                <?endforeach;?>
            </div><!-- [ slides ] -->
            <div class="data">
                <div class="description">
                    <?$i = 0;?>
                    <?foreach($arResult["PROPERTIES"]["SLIDER"]["DESCRIPTION"] as $desc):?>
                        <div class="item <?if($i==0):?>activ<?endif;?>">
                            <?if (strlen($desc) > 0):?>
                                <?=$desc;?>
                            <?else:?>
                                <p></p>
                            <?endif;?>
                        </div><!-- [ item ] -->
                        <?$i++;?>
                    <?endforeach;?>
                </div><!-- [ description ] -->
                <?if(count($arResult["PROPERTIES"]["SLIDER"]["VALUE"])>1):?>
	                <div class="controller">
	                    <button class="prev"></button>
	                    <div class="counter"></div>
	                    <button class="next"></button>
	                </div><!-- [ controller ] -->
                <?endif;?>
            </div><!-- [ data ] -->
        </div><!-- [ photo-slider ] -->
    <?endif;?>

	<div class="column">
    	<?=$arResult["PREVIEW_TEXT"]?>
    </div><!-- [ column ] -->
</div><!-- [ wrapper ] -->

<?if($arResult["PROPERTIES"]["VIDEO"]["VALUE"]):?>
	<?foreach($arResult["PROPERTIES"]["VIDEO"]["VALUE"] as $k => $value):?>
	<div class="video-block _1">
	    <div class="wrap">
	        <div class="trigger"><span>Смотреть</span> <?=$arResult["PROPERTIES"]["VIDEO"]["DESCRIPTION"][$k]?></div>
	        <div class="box">
	        	<?if ((preg_match("/youtube.com/i", $value)) || (preg_match("/youtu.be/i", $value))) 
				{
					if(!preg_match("/embed/i", $value)){
						if(preg_match("/youtu.be/i", $value)){
							$id = explode("youtu.be/", $value);
							$id = $id[1];
						}else{
							$id = explode("youtube.com/watch?v=", $value);
							$id = explode("&", $id[1]);
							$id = $id[0];
						}
					}
					else{
						$id = explode("www.youtube.com/embed/", $value);
						$id = explode("&quot;", $id[1]);
						$id = $id[0];
					}?>
				<?}?> 
	            <iframe width="1180" height="665" src="http://www.youtube.com/embed/<?=$id?>" frameborder="0" allowfullscreen></iframe>
	        </div><!-- [ box ] -->
	    </div><!-- [ wrap ] -->
	</div><!-- [ video-block ] -->
	<?endforeach;?>
<?endif;?>

<div class="wrapper">
	<div class="column">
        <?=$arResult["DETAIL_TEXT"]?>
    </div><!-- [ column ] -->
</div><!-- [ wrapper ] -->

<div class="form-section _1">
	<div class="display begin">
        <div class="wrap">
            <p><?=$arResult["PROPERTIES"]["TEXT"]["VALUE"]?></p>
            <button class="_1" type="button">Запросить стоимость</button>
        </div><!-- [ wrap ] -->
    </div><!-- [ begin ] -->
	<?$APPLICATION->IncludeComponent("custom:main.feedback", "", array(
        "USE_CAPTCHA" => "N",
        "OK_TEXT" => "Спасибо за вашу заявку.",
        "EMAIL_TO" => "ald@redgraphic.com",
        "REQUIRED_FIELDS" => array(
            0 => "NAME",
            1 => "PHONE",
        ),
        "EVENT_MESSAGE_ID" => array(
            0 => "7",
        )
        ),
        $component
    );?>
</div><!-- [ form-section ] -->

<div class="wrapper">
	<div class="column">
        <nav class="file-list">
            <ul>
            	<?
            	$sizes = array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb', 'zb', 'yb'); 
            	if ($arResult["PROPERTIES"]["FILES"]["VALUE"] != ""):
            	?>
		            <nav class="file-list">
		                <ul>
		                	<?foreach($arResult["PROPERTIES"]["FILES"]["VALUE"] as $f => $value):?>
			                    <li>
			                        <?
			                        	$size = 0;
										$i = 0;
			                            $arFile=CFile::MakeFileArray($value);
			                            $size=(int)$arFile["size"];
			                            while($size > 1024)
			                            {
			                                $size = $size / 1024;
			                                $i++;
			                            }
			                        ?>
			                        <a class="link-back" href="<?=CFile::GetPath($value)?>" download=""><?=$arResult['PROPERTIES']['FILES']['DESCRIPTION'][$f]?>, <span><?=end(explode(".", $arFile["name"]));?> – <?=round($size)?> <?=$sizes[$i]?></span></a>
			                    </li>
		                    <?endforeach;?>
		                </ul>
		            </nav><!-- [ file-list ] -->
		        <?endif;?>
            </ul>
        </nav><!-- [ file-list ] -->
    </div><!-- [ column ] -->
</div><!-- [ wrapper ] -->