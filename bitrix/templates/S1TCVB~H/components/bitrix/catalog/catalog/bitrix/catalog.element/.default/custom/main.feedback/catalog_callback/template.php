<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

    <div class="display message">
        <?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
            <div class="done">
                <div class="wrap">
                    <div class="title">Спасибо за вашу заявку.</div>
                    <p>В течении суток с Вами свяжется наш менеджер.</p>
                    <?//=$arResult["OK_MESSAGE"]?>
                </div><!-- [ wrap ] -->
            </div><!-- [ done ] -->
        <?}?>
        <?if(!empty($arResult["ERROR_MESSAGE"])){?>
            <div class="error">
                <div class="wrap">
                    <div class="title">Сообщение не было отправлено</div>
                    <p>Попробуйте повторить попытку отправки формы еще раз.</p>
                    <?
                        //foreach($arResult["ERROR_MESSAGE"] as $v)
                            //ShowError($v);
                    ?>
                </div><!-- [ wrap ] -->
            </div><!-- [ error ] -->
        <?}?>
        <button class="close" type="button">&times;</button>
    </div><!-- [ message ] -->
    <div class="display form">
       <form id="order-call-form" action="<?=POST_FORM_ACTION_URI?>" method="POST">
        <?=bitrix_sessid_post()?>
            <div class="wrap">
                <div class="title">Заказать звонок</div>
                <div class="row _1">
                    <div class="cell">
                        <label>Ваше имя или название кампании:</label>
                        <input class="_letters" name="user_name" type="text" value="" required>
                    </div><!-- [ cell ] -->
                    <div class="cell">
                        <label>Номер телефона:</label>
                        <input class="_phone" name="user_phone" type="text" value="" required>
                    </div><!-- [ cell ] -->
                </div><!-- [ row ] -->
                 <button class="_1 disabled" type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>"><?=GetMessage("MFT_SUBMIT")?></button>
            </div><!-- [ wrap ] -->
            <button class="close" type="button">&times;</button>
        </form><!-- [ order-call-form ] -->
    </div><!-- [ form ] -->