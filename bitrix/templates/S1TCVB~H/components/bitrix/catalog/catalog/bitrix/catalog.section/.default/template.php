<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>

<div class="wrapper">
	<div class="column">
        <h1 class="page-title"><?=$arResult['SECTION']['NAME']?></h1>
    	<nav class="filter">
        	<span class="activ" data-filter="all"><i>все модели</i></span>
        	<?$ar_res = array();?>
          	<?foreach ($arResult['ITEMS'] as $arItem):?>
	        	<?$ar_res[$arItem['ID']] = $arItem['PROPERTIES']['FILTER']['DESCRIPTION'];?>
	        <?endforeach;?>

	        <?
        	$ar_res = array_unique($ar_res);
        	sort($ar_res, SORT_STRING);
      
	        $res = CIBlockElement::GetByID($arResult['ITEMS'][0]['PROPERTIES']['FILTER']['VALUE']);
			if($res = $res->GetNext()){
			  	foreach ($ar_res as $ar):?>
			  		<span data-filter=".<?=$res["CODE"]?>-<?=$ar?>"><i><?=$ar?></i></span>
			  	<?endforeach;?>
			<?}?>
        </nav><!-- [ products-filter ] -->
    </div><!-- [ column ] -->

    <?if (!empty($arResult['ITEMS'])):?>
	    <div class="products-box filtered">
	    	<?foreach ($arResult['ITEMS'] as $arItem):?>
	    		<?
	    			$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
					$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
					$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
				?>
		        <a class="item <?=$res["CODE"]?>-<?=$arItem['PROPERTIES']["FILTER"]["DESCRIPTION"]?>" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
		            <div class="image">
		                <div class="middle">
		                    <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
		                </div><!-- [ middle ] -->
		            </div><!-- [ image ] -->
		            <div class="data">
		                <div class="name"><?=$arItem["NAME"]?></div>
		                <div class="info">
		                	<?$rs = ""?>
		                	<?foreach ($arItem['PROPERTIES']["MODELS"]["VALUE"] as $models):?>
		                		<?
		                			$rs .= $models.",";
		                		?>
		                	<?endforeach;?>
		                	<?echo substr($rs, 0, strlen($rs)-1);?>
		                </div>
		            </div><!-- [ data ] -->
		        </a><!-- [ item ] -->
	        <?endforeach;?>
	    </div><!-- [ products-box ] -->
    <?endif;?>
</div><!-- [ wrapper ] -->