<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<?if (CModule::IncludeModule("iblock")):
	$db_props = CIBlockElement::GetProperty(9, 35, "sort", "asc", Array("CODE"=>"FOOTER_DESC_COMPANY"));
	if($ar_props = $db_props->Fetch()):
		$arResult["FOOTER_DESC_COMPANY"] = $ar_props;
	endif;
endif;?>