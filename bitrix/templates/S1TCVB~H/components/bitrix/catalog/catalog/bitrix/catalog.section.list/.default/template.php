<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>

<pre style="display:none;"><?print_r($arResult['SECTIONS'])?></pre>

<div class="wrapper">
	<div class="column">
        	<h1 class="page-title"><?=$arResult['SECTION']['NAME']?></h1>
    	</div><!-- [ column ] -->

    <?if (0 < $arResult["SECTIONS_COUNT"]):?>
	    <div class="products-box">
	    	<?foreach ($arResult['SECTIONS'] as &$arSection):?>
	    		<?
	    		$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);
				?>
		        <a class="item power-1" href="<?=$arSection["SECTION_PAGE_URL"]?>">
		            <div class="image">
		                <div class="middle">
		                    <img src="<?=$arSection["PICTURE"]["SRC"]?>" alt="<?=$arSection["NAME"]?>">
		                </div><!-- [ middle ] -->
		            </div><!-- [ image ] -->
		            <div class="data">
		                <div class="name"><?=$arSection["NAME"]?></div>
		            </div><!-- [ data ] -->
		        </a><!-- [ item ] -->
	        <?endforeach;?>
	    </div><!-- [ products-box ] -->
    <?endif;?>
</div><!-- [ wrapper ] -->