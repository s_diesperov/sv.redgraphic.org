<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>

<div class="feedback-form">
	<div class="display form">
        <form id="feedback-form" action="<?=POST_FORM_ACTION_URI?>" method="POST">
        <?=bitrix_sessid_post()?>
            <div class="title">написать письмо</div>
            <label>Ваше имя или название кампании:</label>
            <input class="_letters" name="user_name" type="text" value="" required>
            <label>Электронная почта:</label>
            <input class="_email" name="user_email" type="text" value="" required>
            <label>Ваше сообщение:</label>
            <textarea class="_letters_digits" name="MESSAGE" required></textarea>
            <button class="_1 disabled" type="submit" name="submit" value="<?=GetMessage("MFT_SUBMIT")?>"><?=GetMessage("MFT_SUBMIT")?></button>
        </form><!-- [ feedback-form ] -->
    </div><!-- [ form ] -->
    <div class="display message">
        <?if(strlen($arResult["OK_MESSAGE"]) > 0){?>
            <div class="done">
                <div class="title">Спасибо за вашу заявку.</div>
                <p>В течении суток с Вами свяжется наш менеджер.</p>
                <?//=$arResult["OK_MESSAGE"]?>
            </div><!-- [ done ] -->
        <?}?>
        <?if(!empty($arResult["ERROR_MESSAGE"])){?>
            <div class="error">
                <div class="title">Сообщение не было отправлен</div>
                <p>Попробуйте повторить попытку отправки формы еще раз.</p>
                 <?/*
                    foreach($arResult["ERROR_MESSAGE"] as $v)
                        ShowError($v);
                */?>
            </div><!-- [ error ] -->
        <?}?>
    </div><!-- [ message ] -->
</div><!-- [ feedback-form ] -->