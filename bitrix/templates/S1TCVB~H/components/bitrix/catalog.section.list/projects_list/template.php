<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));
?>

<pre style="display:none;"><?print_r($arResult)?></pre>

<!-- [> CONTENT <] -->
<div class="content">
    <div class="wrapper">
        <h1 class="page-title">Реализованные проекты</h1>
    </div><!-- [ wrapper ] -->
    
    <?if ($arResult["SECTIONS_COUNT"] > 0){?>
        <div class="projects-box">
            <?foreach ($arResult['SECTIONS'] as $arSection):?>
                <a class="item" href="<?=$arSection['SECTION_PAGE_URL']?>">
                    <div class="data _1 left show">
                        <div class="top">
                            <div class="title"><?=$arSection["~NAME"]?></div>
                        </div><!-- [ top ] -->
                        <div class="bottom">
                            <p><?=$arSection["UF_PROJECTS_CLIENT"]?></p>
                        </div><!-- [ bottom ] -->
                    </div><!-- [ data show ] -->
                    <div class="data hide">
                        <div class="bottom">
                            <div class="middle">
                                <?=$arSection['DESCRIPTION']?>
                            </div><!-- [ middle ] -->
                        </div><!-- [ bottom ] -->
                    </div><!-- [ data hide ] -->
                    <div class="image">
                         <img src="<?=$arSection['PICTURE']['SRC'];?>" alt="<?=$arSection["NAME"]?>">
                    </div><!-- [ image ] -->
                </a><!-- [ item ] -->
            <?endforeach;?>
        </div><!-- [ projects-box ] -->
    <?}?>
</div><!-- [ content ] -->
<!-- [> CONTENT <] -->