<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>

<?
if (CModule::IncludeModule("iblock")):
	$arSelect = Array('ID', 'IBLOCK_ID', 'NAME', 'CODE', 'DESCRIPTION', 'PICTURE', 'UF_PROJECTS_CLIENT', 'UF_DISPLAY_IN_WHITE');
	$arFilter = Array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'GLOBAL_ACTIVE'=>'Y', 'DEPTH_LEVEL'=>1);
	$rsSections = CIBlockSection::GetList(Array('sort'=>'asc', 'sort'=>'asc'), $arFilter, array(), $arSelect, array());

	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "CODE", "DETAIL_TEXT", "PROPERTY_STAGE", "PROPERTY_SLIDER");

	while ($arSection = $rsSections->Fetch())
	{
		$arResult["PROJECTS_SECTIONS"][$arSection['ID']] = $arSection;

		$arFilter = Array("IBLOCK_ID"=>$arParams['IBLOCK_ID'], "SECTION_CODE"=>$arSection["CODE"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		$rsElements = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
		while($ob = $rsElements->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$arResult["PROJECTS_SECTIONS"][$arSection["ID"]]["ELEMENTS"][$arFields["ID"]] = $arFields;
			$arProps = $ob->GetProperties();
			$arResult["PROJECTS_SECTIONS"][$arSection["ID"]]["ELEMENTS"][$arFields["ID"]]["PROPERTIES"] = $arProps;
		}
	}

	foreach($arResult["PROJECTS_SECTIONS"] as $arSect):
		$arResult["PROJECTS_SECTIONS"][$arSect["ID"]]["PICTURE"] = CFile::GetPath($arSect["PICTURE"]);
		foreach($arSect["ELEMENTS"] as $arElem):

			$res = CIBlockElement::GetByID($arElem["PROPERTIES"]["STAGE"]["VALUE"]);
			if($ar_res = $res->GetNext()):
				$arResult["PROJECTS_SECTIONS"][$arSect["ID"]]["ELEMENTS"][$arElem["ID"]]["PROPERTY_STAGE_VALUE"] = array();
				$arResult["PROJECTS_SECTIONS"][$arSect["ID"]]["ELEMENTS"][$arElem["ID"]]["PROPERTY_STAGE_VALUE"]["NAME"] = $ar_res["NAME"];
				$arResult["PROJECTS_SECTIONS"][$arSect["ID"]]["ELEMENTS"][$arElem["ID"]]["PROPERTY_STAGE_VALUE"]["CODE"] = $ar_res["CODE"];
				$arResult["PROJECTS_SECTIONS"][$arSect["ID"]]["ELEMENTS"][$arElem["ID"]]["PROPERTY_STAGE_VALUE"]["PREVIEW_PICTURE"] = CFile::GetPath($ar_res["PREVIEW_PICTURE"]);
			endif;
		endforeach;
	endforeach;
endif;
?>

<pre><?//print_r($arResult["PROJECTS_SECTIONS"])?></pre>