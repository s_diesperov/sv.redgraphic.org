<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?if($APPLICATION->GetCurDir() === $arResult["LIST_PAGE_URL"]):?>
    <div class="wrapper">
        <h1 class="page-title"><?$APPLICATION->ShowTitle(false);?></h1>
    </div><!-- [ wrapper ] -->
    <div class="projects-box">
    	<?$i=1;?>
		<?foreach($arResult["PROJECTS_SECTIONS"] as $arProj):?>
            <a class="item" href="<?=$arResult["LIST_PAGE_URL"]."".$arProj["CODE"]."/"?>">
                <div class="data <?if($arProj["UF_DISPLAY_IN_WHITE"] != 1):?>_1<?else:?>_2<?endif;?> <?if($i%2 == 0):?>right<?else:?>left<?endif;?> show">
                    <div class="top">
                        <div class="title"><?=$arProj["NAME"]?></div>
                    </div><!-- [ top ] -->
                    <div class="bottom">
                        <p><?=$arProj["UF_PROJECTS_CLIENT"]?></p>
                    </div><!-- [ bottom ] -->
                </div><!-- [ data show ] -->
                <div class="data hide">
                    <div class="bottom">
                        <div class="middle">
                            <?=$arProj["DESCRIPTION"]?>
                        </div><!-- [ middle ] -->
                    </div><!-- [ bottom ] -->
                </div><!-- [ data hide ] -->
                <div class="image">
                     <img src="<?=$arProj["PICTURE"]?>" alt="<?=$arProj["NAME"]?>">
                </div><!-- [ image ] -->
            </a><!-- [ item ] -->
            <?$i++;?>
        <?endforeach;?>
    </div><!-- [ projects-box ] -->
<?else:?>
	<?foreach($arResult["PROJECTS_SECTIONS"] as $arProj):?>
		<?if($APPLICATION->GetCurDir() == $arResult["LIST_PAGE_URL"]."".$arProj["CODE"]."/"){?>
			<div class="wrapper">
		    	<div class="column">
		            <h1 class="page-title"><?=$arProj["NAME"]?></h1>
		            <h3 class="page-subtitle"><?=$arProj["UF_PROJECTS_CLIENT"]?></h3>
		        </div><!-- [ column ] -->
		    </div><!-- [ wrapper ] -->
			
		    <div class="full-size-image">
		    	<img src="<?=$arProj['PICTURE'];?>" alt="<?=$arProj["NAME"]?>">
		    </div><!-- [ full-size-image ] -->
		    
		    <?if(count($arProj["ELEMENTS"]) > 0):?>
			    <nav class="part">
			        <ul>
			        	<?$i=1;?>
			        	<?foreach($arProj["ELEMENTS"] as $arElem):?>
				            <li>
				                <a href="#<?=$arElem["PROPERTY_STAGE_VALUE"]["CODE"]?>">
				                    <i class="_<?=$i?>"></i>
				                    <span><?=$arElem["PROPERTY_STAGE_VALUE"]["NAME"]?></span>
				                </a>
				            </li>
				           <?$i++;?>
			            <?endforeach;?>
			        </ul>
			    </nav><!-- [ part ] -->
		    <?endif;?>
		    
		    <div class="wrapper">
			    <?foreach($arProj["ELEMENTS"] as $arElem):?>
			    	<div class="column">
			            <section class="spy" id="<?=$arElem["PROPERTY_STAGE_VALUE"]["CODE"]?>">
			                <h2><?=$arElem["NAME"]?></h2>
			                <?=$arElem["DETAIL_TEXT"]?>
			            </section><!-- [ task ] -->
			        </div><!-- [ column ] -->

			   		<?if($arElem["PROPERTIES"]["SLIDER"]["VALUE"]):?>
				        <div class="photo-slider">
							<div class="slides">
								<?foreach($arElem["PROPERTIES"]["SLIDER"]["VALUE"] as $sl => $value):?>
								    <div class="item"><img src="<?=CFile::GetPath($value)?>" alt="<?=$arResult["PROPERTIES"]["SLIDER"][$sl]["DESCRIPTION"]?>"></div>
							    <?endforeach;?>
							</div><!-- [ slides ] -->
							<div class="data">
							    <div class="description">
							    	<?$i = 0;?>
							    	<?foreach($arElem["PROPERTIES"]["SLIDER"]["DESCRIPTION"] as $desc):?>
								        <div class="item <?if($i==0):?>activ<?endif;?>">
								            <?if (strlen($desc) > 0):?>
								            	<?=$desc;?>
								            <?else:?>
								            	<p></p>
								            <?endif;?>
								        </div><!-- [ item ] -->
								        <?$i++;?>
							        <?endforeach;?>
							    </div><!-- [ description ] -->
							    <div class="controller">
							        <button class="prev"></button>
							        <div class="counter"></div>
							        <button class="next"></button>
							    </div><!-- [ controller ] -->
							</div><!-- [ data ] -->
			            </div><!-- [ photo-slider ] -->
		            <?endif;?>
			    <?endforeach;?>
		    	<div class="column">
		    		<nav class="link-list">
		            	<ul>
		                	<li><a class="link-back" href="<?=$arResult["LIST_PAGE_URL"]?>">Вернуться ко всем проектам</a></li>
		                </ul>
		            </nav><!-- [ link-list ] -->
		    	</div>
		    </div><!-- [ wrapper ] -->		    
		<?}?>
	<?endforeach;?>
<?endif;?>