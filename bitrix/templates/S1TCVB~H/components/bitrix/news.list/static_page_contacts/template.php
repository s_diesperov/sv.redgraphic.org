<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="wrapper">
    <h1 class="page-title"><?=$arParams["IBLOCK_NAME"]?></h1>
    <div class="contacts-box">
        <nav>
            <?$i = 1;?>
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <span <?if($i == 1):?>class="activ"<?endif;?> data-office="<?=$i?>"><i><?=$arItem["NAME"]?></i></span>
                <?$i++;?>
            <?endforeach;?>
        </nav>

        <div class="container">
            <div class="tabs">
                <?$i = 1;?>
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <div class="tab<?if($i == 1):?> activ<?endif;?>">
                        <div class="data">
                            <?=$arItem["PREVIEW_TEXT"]?>
                        </div><!-- [ data ] -->
                    </div><!-- [ tab ] -->
                    <?$i++;?>
                <?endforeach;?>
            </div><!-- [ tabs ] -->
            <div id="map" style="float: right;width: 69.55%;height: 415px;background: #f3f3f3;">
                <?
                $arPos = explode(',', $arItem["PROPERTIES"]["GMAP"]["VALUE"][0]);
                $arRes = array();
                $i = 0;
                foreach($arItem["PROPERTIES"]["GMAP"]["VALUE"] as $gmap):
                    $arPos = explode(',', $gmap);
                    $arRes[$i] = array('TEXT' => "!!!!!",'LON' => $arPos[1],'LAT' => $arPos[0]);
                    $i++;
                endforeach;
                ?>
                <?$APPLICATION->IncludeComponent("bitrix:map.google.view", ".default", array(
	"INIT_MAP_TYPE" => "ROADMAP",
	"MAP_DATA" => serialize(array("google_lat"=>$arPos[0],"google_lon"=>$arPos[1],"google_scale"=>13,"PLACEMARKS"=>$arRes,)),
	"MAP_WIDTH" => "100%",
	"MAP_HEIGHT" => "415px",
	"CONTROLS" => array(
		0 => "SMALL_ZOOM_CONTROL",
		1 => "SCALELINE",
	),
	"OPTIONS" => array(
		0 => "ENABLE_DRAGGING",
	),
	"MAP_ID" => ""
	),
	false
);?> 

            </div>
        </div><!-- [ container ] -->
    </div><!-- [ contacts-box ] -->
</div><!-- [ wrapper ] -->
