<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="partners-block">
        <div class="wrap">
            <div class="partners-slider">
                <div class="slides">
                    <?$i=1;?>
                    <?foreach($arResult["ITEMS"] as $arItem):?>
                        <div class="item">
                            <a class="link" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" target="_blank">
                                <div class="image activ">
                                    <div class="middle">
                                        <img src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
                                    </div><!-- [ middle ] -->
                                </div><!-- [ image ] -->
                                <div class="image default">
                                    <div class="middle">
                                        <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>">
                                    </div><!-- [ middle ] -->
                                </div><!-- [ image ] -->
                            </a><!-- [ link ] -->
                        </div><!-- [ item ] -->
                        <?$i++;?>
                    <?endforeach;?>
                </div><!-- [ slides ] -->
            </div><!-- [ partners-slider ] -->
        </div><!-- [ wrap ] -->
    </div><!-- [ partners-block ] -->