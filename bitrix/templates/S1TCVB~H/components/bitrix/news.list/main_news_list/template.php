<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="last-news-block">
    <div class="items">
        <?foreach($arResult["ITEMS"] as $arItem):?>
             <a class="item" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                <div class="date"><?=$arItem["DATE_ACTIVE_FROM"]?></div>
                <div class="title"><?=$arItem["NAME"]?></div>
                <p><?=$arItem["PREVIEW_TEXT"]?></p>
            </a><!-- [ item ] -->
        <?endforeach;?>
    </div><!-- [ items ] -->
    <a class="link-part" href="/news/">ВСЕ НОВОСТИ</a>
</div><!-- [ last-news-block ] -->