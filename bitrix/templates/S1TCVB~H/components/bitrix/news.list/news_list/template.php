<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?foreach($arResult["ITEMS"] as $arItem):?>
	<article class="item" data-id="<?=$arItem["ID"]?>">
	    <div class="date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
	    <div class="data">
	        <a class="title" href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a>
	        <p><?=$arItem["PREVIEW_TEXT"]?></p>
	    </div><!-- [ data ] -->
	</article><!-- [ item ] -->
<?endforeach;?>