<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="directions-block">
    <?foreach($arResult["ITEMS"] as $arItem):?>
         <a class="item" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>">
            <div class="data">
                <div class="title"><?=$arItem["NAME"]?></div>
            </div><!-- [ data ] -->
            <div class="image" style="background-image:url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);background-color:#<?=$arItem["PROPERTIES"]["BACK"]["VALUE"]?>;"></div>
        </a><!-- [ item ] -->
    <?endforeach;?>
</div><!-- [ directions-block ] -->