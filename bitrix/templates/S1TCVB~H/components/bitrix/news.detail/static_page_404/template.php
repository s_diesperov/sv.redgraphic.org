<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="wrapper">
    <div class="section-error">
    	<div class="data">
        	<h1><?=$arResult["NAME"]?></h1>
           	<?=$arResult["PREVIEW_TEXT"]?>
        </div><!-- [ data ] -->
        <div class="image">
        	<img class="float-right" src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>" width="409" height="409" alt="404">
        </div><!-- [ image ] -->
    </div><!-- [ section-error ] -->
</div><!-- [ wrapper ] -->
