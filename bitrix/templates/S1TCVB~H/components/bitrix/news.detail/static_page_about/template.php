<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<div class="wrapper">
    <div class="column">
        <h1 class="page-title"><?=$arResult["NAME"]?></h1>
        
        <div class="page-date"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></div>
        <?if(strlen($arResult["DETAIL_TEXT"]) > 0):echo $arResult["PREVIEW_TEXT"];else:?><p><?=$arResult["DETAIL_TEXT"];?></p><?endif;?>
    </div><!-- [ column ] -->
    
    <?if($arResult["PROPERTIES"]["SLIDER"]["VALUE"]):?>
        <div class="photo-slider">
            <div class="slides">
                <?foreach($arResult["PROPERTIES"]["SLIDER"]["VALUE"] as $sl => $value):?>
                    <div class="item"><img src="<?=CFile::GetPath($value)?>" alt="<?=$arResult["PROPERTIES"]["SLIDER"][$sl]["DESCRIPTION"]?>"></div>
                <?endforeach;?>
            </div><!-- [ slides ] -->
            <div class="data">
                <div class="description">
                    <?$i = 0;?>
                    <?foreach($arResult["PROPERTIES"]["SLIDER"]["DESCRIPTION"] as $desc):?>
                        <div class="item <?if($i==0):?>activ<?endif;?>">
                            <?if (strlen($desc) > 0):?>
                                <?=$desc;?>
                            <?else:?>
                                <p></p>
                            <?endif;?>
                        </div><!-- [ item ] -->
                        <?$i++;?>
                    <?endforeach;?>
                </div><!-- [ description ] -->
                <div class="controller">
                    <button class="prev"></button>
                    <div class="counter"></div>
                    <button class="next"></button>
                </div><!-- [ controller ] -->
            </div><!-- [ data ] -->
        </div><!-- [ photo-slider ] -->
    <?endif;?>
    
    <div class="column">
        <?=$arResult["DETAIL_TEXT"]?>
        <?
        $sizes = array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb', 'zb', 'yb'); 
        $size = 0;
        $i = 0;
        ?>
        <?if ($arResult["PROPERTIES"]["FILE"]["VALUE"] != ""):?>
            <nav class="file-list">
                <ul>
                    <li>
                        <?
                            $arFile=CFile::MakeFileArray($arResult['PROPERTIES']['FILE']['VALUE']);
                            $size=(int)$arFile["size"];
                            while($size > 1024)
                            {
                                $size = $size / 1024;
                                $i++;
                            }
                        ?>
                        <a class="link-back" href="<?=CFile::GetPath($arResult['PROPERTIES']['FILE']['VALUE'])?>" download=""><?=$arResult['PROPERTIES']['FILE']['DESCRIPTION']?>, <span><?=end(explode(".", $arFile["name"]));?> – <?=round($size)?> <?=$sizes[$i]?></span></a>
                    </li>
                </ul>
            </nav><!-- [ file-list ] -->
        <?endif;?>
    </div><!-- [ column ] -->
</div><!-- [ wrapper ] -->