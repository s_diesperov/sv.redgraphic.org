
/*[Map marker ico <]*/
var map_marker_ico = [
	[
		'img/elements/ui/site/single/marker-1-1.png',
		'img/elements/ui/site/single/marker-1-2.png',
	]
];
/*[Map marker ico >]*/

/*[Map office data <]*/
var map_office_data = {
	1: {
		coords: [53.9, 27.56666670000004],
		map_marker_ico: 'img/elements/ui/site/single/marker-1-1.png',
		full_address: 'Беларусь, Минск, пр-т Победителей, 23/3',
		description: '<div class="title">Офис №1</div>'+
					 '<p>'+
						 'Понедельник–Пятница: 9:00–17:00. Кассы для частных клиентов работают до 20:00.<br>'+
						 'Суббота: 10:00–16:00 — только для частных клиентов.<br>'+
						 'Воскресенье: выходной'+
					 '</p>'
	},
	2: {
		coords: [53.9211883, 27.578333199999975],
		map_marker_ico: 'img/elements/ui/site/single/marker-1-1.png',
		full_address: 'Беларусь, Минск, пр-т Победителей, 23/3',
		description: '<div class="title">Офис №1</div>'+
					 '<p>'+
						 'Понедельник–Пятница: 9:00–17:00. Кассы для частных клиентов работают до 20:00.<br>'+
						 'Суббота: 10:00–16:00 — только для частных клиентов.<br>'+
						 'Воскресенье: выходной'+
					 '</p>'
	},
	3: {
		coords: [53.911068, 27.54053],
		map_marker_ico: 'img/elements/ui/site/single/marker-1-1.png',
		full_address: 'Беларусь, Минск, пр-т Победителей, 23/3',
		description: '<div class="title">Офис №1</div>'+
					 '<p>'+
						 'Понедельник–Пятница: 9:00–17:00. Кассы для частных клиентов работают до 20:00.<br>'+
						 'Суббота: 10:00–16:00 — только для частных клиентов.<br>'+
						 'Воскресенье: выходной'+
					 '</p>'
	}
};
/*[Map office data >]*/


