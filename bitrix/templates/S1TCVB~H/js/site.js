var currPage = 1;
var count;

$(function() {
	$.fn.equalizeHeights = function() {
		var maxHeight = this.map(function(i,e) {
			return $(e).height();
		}).get();
		return this.height(Math.max.apply(this, maxHeight));
	};
});

var _GET = function(prop) {
	var URL = document.location.href;
	var GET = JSON.parse('{"'+URL.substring(URL.indexOf('?') + 1).replace(/&/, '","').replace(/=/g, '":"')+'"}');
	if (prop) {return GET[prop];} else {return GET;}
};

if (typeof Array.prototype.forEach != 'function') {
    Array.prototype.forEach = function(callback){
      for (var i = 0; i < this.length; i++){
        callback.apply(this, [this[i], i, this]);
      }
    };
};

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(elt /*, from*/) {
		var len = this.length >>> 0;
		var from = Number(arguments[1]) || 0;
		from = (from < 0)
			? Math.ceil(from)
				: Math.floor(from);
		if (from < 0)
			from += len;
		
		for (; from < len; from++) {
			if (from in this && this[from] === elt)
				return from;
		}
		return -1;
	};
};

$(function() {
	
	moment.locale('ru');
	
	/*[===HEADER <]*/
	
	/*[Search <]*/
	if ($('header.main .search').length) {
		$('header.main .search .trigger').click(function(e) {
			var button = $(this),
				parent = button.parents('.search'),
				field = parent.find(':text'),
				form = parent.find('.form');
			
			if (!parent.hasClass('open')) {
				TweenMax.to(form, .6, {opacity: 1, display: 'block'});
				setTimeout(function() {field.focus()}, 150);
				parent.addClass('open');
			} else {
				TweenMax.to(form, .4, {opacity: 0, display: 'none', onComplete: function() {
					parent.removeClass('open');
				}});
			}
		});
		
		/*$(document).on('click', function(e) {
			if ($(e.target).closest('header.main .search').length == 0) {
				TweenMax.to('header.main .search .form', .4, {opacity: 0, display: 'none', onComplete: function() {
					$('header.main .search').removeClass('open');
				}});
			}
		});*/
	};
	
	if ($('.search').length) {
	
		/*[Request <]*/
		var searchTimeout;
		$('.search input[type="text"]').on('keyup keydown keypress', function(e) {
			var $this = $(this),
				parent = $this.parents('.search'),
				button_close = parent.find('button.close'),
				results = $('.search-results'),
				field = parent.find(':text'),
				query = field.val();
				
			clearTimeout(searchTimeout);
			
			if (query.replace(/\s+/g, '').length >= 3) {
				searchTimeout = setTimeout(function() {
					$.ajax({
						url: '#',
						cache: true,
						type: 'post',
						data: {
							query: query
						},
						beforeSend: function() {
							field.addClass('loading');
						},
						success: function(answer) {
							answer = {
								amount: 5,
								results:
									'<a class="item" href="catalog-item.html">'+
										'<div class="image">'+
											'<div class="middle">'+
												'<img src="img/images/group-8/1.jpg" alt="">'+
											'</div><!-- [ middle ] -->'+
										'</div><!-- [ image ] -->'+
										'<div class="data">'+
											'<div class="name">case ih</div>'+
											'<div class="info">steiger 600, quadtrac 600</div>'+
										'</div><!-- [ data ] -->'+
									'</a><!-- [ item ] -->'+
									'<a class="item" href="catalog-item.html">'+
										'<div class="image">'+
											'<div class="middle">'+
												'<img src="img/images/group-8/2.jpg" alt="">'+
											'</div><!-- [ middle ] -->'+
										'</div><!-- [ image ] -->'+
										'<div class="data">'+
											'<div class="name">case ih</div>'+
											'<div class="info">steiger 600/550, quadtrac 500/550</div>'+
										'</div><!-- [ data ] -->'+
									'</a><!-- [ item ] -->'+
									'<a class="item" href="catalog-item.html">'+
										'<div class="image">'+
											'<div class="middle">'+
												'<img src="img/images/group-8/1.jpg" alt="">'+
											'</div><!-- [ middle ] -->'+
										'</div><!-- [ image ] -->'+
										'<div class="data">'+
											'<div class="name">case ih</div>'+
											'<div class="info">steiger 400/500, quadtrac 450</div>'+
										'</div><!-- [ data ] -->'+
									'</a><!-- [ item ] -->'+
									'<a class="item" href="catalog-item.html">'+
										'<div class="image">'+
											'<div class="middle">'+
												'<img src="img/images/group-8/3.jpg" alt="">'+
											'</div><!-- [ middle ] -->'+
										'</div><!-- [ image ] -->'+
										'<div class="data">'+
											'<div class="name">new holland t9</div>'+
											'<div class="info">t9.670</div>'+
										'</div><!-- [ data ] -->'+
									'</a><!-- [ item ] -->'+
									'<a class="item" href="catalog-item.html">'+
										'<div class="image">'+
											'<div class="middle">'+
												'<img src="img/images/group-8/4.jpg" alt="">'+
										   '</div><!-- [ middle ] -->'+
										'</div><!-- [ image ] -->'+
										'<div class="data">'+
											'<div class="name">new holland t9</div>'+
											'<div class="info">t9.560/t9.615</div>'+
										'</div><!-- [ data ] -->'+
									'</a><!-- [ item ] -->'
							};
							
							results.html(
								'<div class="wrap">'+
									'<h2>Найдено результатов: '+answer.amount+'</h2>'+
									'<div class="products-box">'+
										answer.results+
									'</div><!-- [ products-box ] -->'+
								'</div><!-- [ wrap ] -->'
							);
							
							field.removeClass('loading');
							results.slideDown(600);
							button_close.show();
						},
						error: function() {
							field.removeClass('loading');
							button_close.hide();
						}
					});
				}, 600);
			};
		});
		
		$('.search input[type="text"]').on('focusin', function(e) {
			var $this = $(this),
				results = $('.search-results'),
				parent = $this.parents('.search'),
				parent = parent,
				button_close = $this.parents('.search').find('button.close');
			if (results.find('.item').length) {
				results.slideDown(600);
				button_close.show();
			}
		});
		
		$(document).on('click', '.search button.close, .search.open .trigger', function(e) {
			var $this = $(this),
				results = $('.search-results'),
				button_close = $this.parents('.search').find('button.close');
			results.slideUp(400);
			button_close.hide();
		});
		/*[Request >]*/
	
	};
	/*[Search >]*/
	
	/*[Nav lang <]*/
	if ($('header.main nav.lang').length) {
		TweenMax.set('header.main nav.lang .dropdown', {yPercent: -105})
		$('header.main nav.lang .trigger').click(function(e) {
			var height = 0,
				$this = $(this),
				parent = $this.parent('nav'),
				dropdown = parent.find('.dropdown');
			
			if (!parent.hasClass('open')) {
				parent.addClass('open');
				TweenMax.to(dropdown, .4, {yPercent: 0});
			} else {
				TweenMax.to(dropdown, .4, {yPercent: -105, onComplete: function() {
					parent.removeClass('open');
				}});
			}
		});
		
		$(document).on('click', function(e) {
			if ($(e.target).closest('nav.lang').length == 0) {
				TweenMax.to('header.main nav.lang .dropdown', .4, {yPercent: -105, onComplete: function() {
					$('header.main nav.lang').removeClass('open');
				}});
			}
		});
	};
	/*[Nav lang >]*/
	
	/*[Nav inner <]*/
	if ($('nav.inner').length) {
		$('nav.inner > ul > li > span').click(function(e) {
			var $this = $(this).parent(),
				trigger = $this.children('span'),
				submenu = $this.children('.submenu'),
				submenu_height = submenu.outerHeight();
			
				if (!trigger.hasClass('active')) {
					if (submenu.length) {
						trigger.addClass('active');
					};
					$('nav.inner > ul > li > span').not(trigger).removeClass('active');
					TweenMax.to('nav.inner .submenu', .4, {opacity: 0, display: 'none'});
					TweenMax.to(submenu, .6, {opacity: 1, display: 'block'})
				} else {
					$('nav.inner > ul > li > span').removeClass('active');
					TweenMax.to('nav.inner .submenu', .4, {opacity: 0, display: 'none'});
				}
		});
		
		$('nav.inner .submenu .item nav ul li a').hover(function(e) {
			$(this).toggleClass('hover').parents('ul').toggleClass('activ')
		});
		
		$(document).on('click', function(e) {
			if ($(e.target).closest('nav.inner').length == 0) {
				$('nav.inner > ul > li > span').removeClass('active');
				TweenMax.to('nav.inner .submenu', .4, {opacity: 0, display: 'none'});
			}
		});
	};
	/*[Nav inner >]*/
	
	/*[Window contact forms <]*/
	if ($('.window-contact-forms').length) {
		TweenMax.set('.window-contact-forms', {top: 90, yPercent: -105});
		
		var windowContactFormsShow = function(e) {
			$('header.main .call').addClass('activ');
			TweenMax.to('.window-contact-forms', .6, {top: 90, yPercent: 0, ease: Power0.easeInOut, onComplete: function() {
				$('.window-contact-forms .tabs .tab.activ input:first').focus();
			}});
		};
		
		var windowContactFormsHide = function(e) {
			TweenMax.to('.window-contact-forms', .6, {top: 90, yPercent: -105, ease: Power0.easeInOut, onComplete: function() {
				$('header.main .call').removeClass('activ');
				$('.window-contact-forms .tabs .tab:eq(0)').addClass('activ').siblings().removeClass('activ');
				$('.window-contact-forms .tabs nav span:eq(0)').addClass('activ').siblings().removeClass('activ');
			}});
		};
		
		$('header.main .call').click(function(e) {
			var $this = $(this);
			if (!$this.hasClass('activ')) {
				windowContactFormsShow();
			} else {
				windowContactFormsHide();
			}
		});
		
		$('.window-contact-forms button.close').click(function(e) {
			windowContactFormsHide();
		});
		
		$(document).on('click', function(e) {
			if ($(e.target).closest('.window-contact-forms, header.main .call').length == 0) {
				windowContactFormsHide();
			}
		});
		
		$('.window-contact-forms .tabs nav span').click(function(e) {
			var $this = $(this)
				index = $this.index(),
				parent = $this.parents('.tabs'),
				tabs = parent.find('.tab');
			tabs.removeClass('activ').eq(index).addClass('activ');
			$this.addClass('activ').siblings().removeClass('activ');
			$('.window-contact-forms .tabs .tab.activ input:first').focus();
		});
	};
	/*[Window contact forms >]*/
	
	/*[===HEADER >]*/
	
	/*[===CONTENT <]*/
	
	/*[=====GROUP <]*/
	
	/*[Box <]*/
	
	/*[Products box <]*/
	if ($('nav.filter').length && $('.products-box').length) {
		$('.products-box').mixItUp({
			controls: {
				activeClass: 'activ',
			},
			animation: {
				duration: 400,
				easing: 'ease',
				effects: 'fade translateZ(-360px) stagger(34ms)'
			},
			selectors: {
				target: '.item',
				filter: 'nav.filter span'
			}
		});
		
		if (_GET('power')) {
			$('.products-box').mixItUp('filter', '.power-'+_GET('power'));
		};
	};
	/*[Products box >]*/
	
	/*[Contacts box <]*/
	$('.contacts-box nav span').click(function(e) {
		var $this = $(this),
			index = $this.index(),
			office = $this.data('office'),
			parent = $this.parents('.contacts'),
			coords = map_office_data[office].coords,
			tabs = parent.find('.tab');
		tabs.removeClass('activ').eq(index).addClass('activ');
		$this.addClass('activ').siblings().removeClass('activ');
		gmap.showPlace(map_office, 16, coords);
	});
	/*[Contacts box >]*/
	
	/*[Projects box <]*/
	$('.projects-box .item').hover(function(e) {
		var $this = $(this),
			data = $this.find('.data.hide .bottom');
		TweenMax.to(data, .6, {bottom: 0});
    }, function(e) {
		var $this = $(this),
			data = $this.find('.data.hide .bottom');
		TweenMax.to(data, .6, {bottom: '-100%'});
    });
	/*[Projects box >]*/
	
	/*[Box >]*/
	
	/*[Block <]*/
	
	/*[Spy block <]*/
	if ($('.spy').length) {
		$('.spy').waypoint(function(direction) {
			var $links = $('a[href="#' + this.id + '"]');
			$links.toggleClass('activ', direction === 'down');
		}, {
        	offset: '100%',
			context: window
		}).waypoint(function(direction) {
			var $links = $('a[href="#' + this.id + '"]');
			$links.toggleClass('activ', direction === 'up');
		}, {
			context: window,
			offset: function() {
			  return -$(this).height() +185;
			}
		});
	};
	/*[Spy block >]*/
	
	/*[Video block <]*/
	$('.video-block .trigger').click(function(e) {
        var $this = $(this),
			parent = $this.parent(),
			box = parent.children('.box'),
			label = $this.children('span');
		if (!$this.hasClass('open')) {
			label.html('Закрыть');
			$this.addClass('open');
		} else {
			label.html('Смотреть');
			$this.removeClass('open');
		};
		box.stop().slideToggle();
    });
	/*[Video block >]*/
	
	/*[Block >]*/
	
	/*[Slider <]*/
	
	/*[Photo slider <]*/
	if ($('.photo-slider').length) {
		var photoSliderAction = function(data) {
			var parent = $(data.$slider).parents('.photo-slider'),
				desc = parent.find('.description .item'),
				counter = parent.find('.counter'),
				slide_current = data.currentSlide,
				slide_amount = data.slideCount;
			
			if (slide_amount > 1) {
				counter.html((slide_current + 1)+'/'+slide_amount);
			};
			
			desc.eq(slide_current).addClass('activ').siblings().removeClass('activ');
		};
		
		$('.photo-slider .slides').slick({
			arrows: false,
			onInit: function(data) {
				photoSliderAction(data);
			},
			onAfterChange: function(data) {
				photoSliderAction(data);
			}
		});
		
		$('.photo-slider .controller .prev').click(function(e) {
			$(this).parents('.photo-slider').find('.slides').slickPrev();
		});
		
		$('.photo-slider .controller .next, .photo-slider .slides .item').click(function(e) {
			$(this).parents('.photo-slider').find('.slides').slickNext();
		});
	};
	/*[Photo slider >]*/
	
	/*[Partners slider <]*/
	if ($('.partners-slider').length) {
		$('.partners-slider .slides').slick({
			slidesToShow: 4,
			slidesToScroll: 1
		});
	};
	/*[Partners slider >]*/
	
	/*[Slider >]*/
	
	/*[Section <]*/
	
	/*[Section excursion <]*/
	if ($('.excursion').length) {
		
		/*[Point <]*/
		$('.excursion .point').click(function(e) {
			var $this = $(this),
				rel = $this.data('relation').split(':'),
				tab = $('.excursion nav.tabs .tab').eq(rel[0]),
				dropdown = tab.find('.dropdown'),
				button = tab.find('button'),
				items = tab.find('.item');
			
			
			if (!$this.hasClass('activ')) {
				$('.excursion .point').removeClass('activ');
				
				$this.addClass('activ');
				items.removeClass('show').addClass('hide');
				tab.addClass('activ').siblings().removeClass('activ');
				items.eq(rel[1]).removeClass('hide').addClass('show');
				TweenMax.to('.excursion nav.tabs .tab .dropdown', .8, {yPercent: 100});
				TweenMax.to('.excursion nav.tabs .tab .close', .2, {opacity: 0, display: 'none'});
				TweenMax.to(button, .2, {opacity: 1, display: 'block', onComplete: function() {
					TweenMax.to(dropdown, .4, {yPercent: -100});
				}});
			} else {
				TweenMax.to('.excursion nav.tabs .tab .close', .2, {opacity: 0, display: 'none'});
				TweenMax.to('.excursion nav.tabs .tab .dropdown', .8, {yPercent: 100});
				$('.excursion nav.tabs .tab').removeClass('activ');
				$this.removeClass('activ');
				items.removeClass('hide');
			}
		});
		/*[Point >]*/
		
		/*[Scene <]*/
		var excursionScene,
			excursionSceneScrollTimeout;
		
		$(window).load(function() {
			excursionScene = new IScroll('.excursion #scene', {
				probeType: 3,
				startX: 0,
				startY: 0,
				bounce: false,
				scrollX: true,
				scrollY: true,
				momentum: true,
				freeScroll: true,
				mouseWheel: false,
				keyBindings: true,
				deceleration: 0.01,
			});
		});
		
		var section_amount = $('.excursion #scene .content .section').length,
			scene_width = section_amount * 1280;
		$('.excursion').css({'max-width': scene_width});
		/*[Scene >]*/
		
		/*[Resize <]*/
		$(window).on('resize', function(e) {
			var h = $(window).height() - $('header.main').height();
			if (h < 486) {h = 486}; 
			if (h > 710) {h = 710}; 
			$('.excursion').height(h);
		}).trigger('resize');
		/*[Resize >]*/
		
		/*[Dropdown <]*/
		$('.excursion nav.tabs .tab').click(function(e) {
			var tab = $(this),
				button = tab.find('.close'),
				dropdown = tab.find('.dropdown');
			if (!button.is(':visible')) {
				TweenMax.to(button, .2, {opacity: 1, display: 'block', ease: Power0.easeIn, onComplete: function() {
					TweenMax.to(dropdown, .4, {yPercent: -100});
				}});
			};
		});
		
		$('.excursion nav.tabs .tab').click(function(e) {
			var tab = $(this),
				button = tab.find('.close'),
				dropdown = tab.find('.dropdown');
			
			TweenMax.to('.excursion nav.tabs .tab .close', .2, {opacity: 0, display: 'none', ease: Linear.easeNone});
			TweenMax.to('.excursion nav.tabs .tab .dropdown', .8, {yPercent: 100, ease: Linear.easeNone, onComplete: function() {
				$('.excursion .point').removeClass('activ');
				$('.excursion nav.tabs .tab .item').removeClass('show hide');
			}});
			
			if (!tab.hasClass('activ')) {
				tab.addClass('activ');
				TweenMax.to(button, .2, {opacity: 1, display: 'block', ease: Power0.easeIn, onComplete: function() {
					TweenMax.to(dropdown, .4, {yPercent: -100});
				}});
			} else {
				tab.removeClass('activ');
			}
			
		});
		
		$('.excursion nav.tabs .tab > button').click(function(e) {
			var button = $(this),
				parent = button.parent('.tab'),
				dropdown = parent.find('.dropdown');
			
			TweenMax.to(dropdown, .4, {yPercent: 100, ease: Linear.easeNone, onComplete: function() {
				TweenMax.to(button, .2, {opacity: 0, display: 'none', ease: Linear.easeNone});
			}});
		});
		/*[Dropdown >]*/
		
	};
	/*[Section excursion >]*/
	
	/*[Section directions <]*/
	if ($('.directions-block').length) {
		$('.directions-block .item').hover(function(e) {
			var obj = $(this), image = obj.find('.image');
			TweenMax.to(image, .2, {top: -20, height: 346});
		}, function(e) {
			var obj = $(this), image = obj.find('.image');
			TweenMax.to(image, .2, {top: 0, height: 306});
		});
	};
	/*[Section directions >]*/
	
	/*[Section >]*/
	
	/*[=====GROUP <]*/
	
	/*[JSON <]*/
	var getDATA = function(path) {
		var value = 0,
			response = $.ajax({
				url: path,
				type: 'POST',
				async: false,
				dataType: 'json',
				data: {type:'request'},
			}).responseText;
		value = $.parseJSON(response);
		return value;
	};
	/*[JSON >]*/
	
	/*[Videos <]*/
	$(window).on('resize', function(e) {
		$('iframe[src*="youtube"]').each(function(index, element) {
			var iframe = $(this),
				width_current = iframe.width(),
				width_init = iframe.attr('width'),
				height_init = iframe.attr('height'),
				height_current = (width_current / width_init) * height_init;
			iframe.css({'height': height_current});
		});
	});
	/*[Videos >]*/
	
	/*[Window <]*/
	$('[data-window]').click(function(e) {
		var name = $(this).data('window');
		$('.window[data-name="'+name+'"]').arcticmodal();
    });
	/*[Window >]*/
	
	/*[Nav section <]*/
	if ($('nav.part').length) {
		$('nav.part').waypoint('sticky', {
			stuckClass: 'fixed'
		});
		$('nav.part a').click(function(e) {
			var $this = $(this),
				target = $this.attr('href'),
				coord = $(target).offset().top;
			$('html, body').stop().animate({scrollTop: coord - 185}, 400);
			return false;
		});
	};
	/*[Nav section >]*/
	
	/*[Graph stages <]*/
	if ($('.graph-stages').length) {
		var project_end   = moment($('.graph-stages').data('end'), 'MM.YYYY'),
			project_begin = moment($('.graph-stages').data('begin'), 'MM.YYYY');
		
		var setGraphStages = function() {
			var month_amount = $('.graph-stages .month').length,
				month_size = $('.graph-stages').width() / month_amount;
			
			$('.graph-stages .month').width(month_size);
			
			$('.graph-stages .item').each(function(index, element) {
				var $this = $(this),
					stage_end   = moment($this.data('end'), 'MM.YYYY'),
					stage_begin = moment($this.data('begin'), 'MM.YYYY'),
					begin = stage_begin.diff(project_begin, 'month'),
					end   = project_end.diff(stage_end, 'month');
				
				$this.css({'margin': '0px '+(end * month_size)+'px 0px '+(begin * month_size)+'px'});
			});
		};
		
		$(window).on('resize', setGraphStages).trigger('resize');
		TweenMax.set('.graph-stages .scene .item', {left: -1180});
		
		$('.graph-stages').waypoint(function() {
			TweenMax.staggerTo('.graph-stages .scene .item', 1, {left: 0, ease: Linear.easeNone}, .2);
		}, { offset: '100%' });
	};
	/*[Graph stages >]*/
	
	/*[Endless scroll <]*/
	var endlessScroll = {
		box: null,
		amount: 5,
		data: null,
		busy : false,
		type: 'post',
		//url: '#get-news',
		url: '/include/ajax_news.php',
		
		init: function(box) {
			var o = this;
			
			if (box) {
				o.box = $(box);
				
				$(window).on('scroll', function(e) {
					if ($(document).height() - $(window).height() <= $(window).scrollTop() + 50) {
						o.scrollPosition = $(window).scrollTop(); o.get();
					}
				});
			}
		},
	 
		get: function() {
			var o = this,
				box = $(o.box),
				loader =  box.find('.loader'),
				last_id =  box.find('article:last').data('id');
				count = $("#news-box").data("count");

			if (!o.busy) {
				if(currPage*5 <= count){
					currPage = currPage + 1;
					$.ajax({
						url: o.url+"?PAGEN_1="+currPage,
						type: o.type,
						data: {
							last: last_id,
							count: o.count
						},
						error: function() {
						},
						beforeSend: function() {
							o.busy = true;
							loader.show();
						},
						success: function(data) {
							loader.before(
								data
							).hide();
							
							if (o.scrollPosition !== undefined && o.scrollPosition !== null) {
								$(window).scrollTop(o.scrollPosition);
							}
							
							o.busy = false;
						},
					});
				}
			}
		}
	};
	
	endlessScroll.init('.news-box');
	/*[Endless scroll >]*/
	
	/*[===CONTENT >]*/
	
	/*[===FORM <]*/
	
	/*[Form reset <]*/
	var formReset = function(form) {
		$(form).validate().resetForm();
		$(form).find('input').iCheck('uncheck');
		$(form).find('input:checked').iCheck('check');
		$(form).find('input[type="text"], textarea').val('');
		$(form).find('select option:first').prop('selected', true);
		$(form).find('select').selectOrDie('update');
	};
	
	$('button:reset').on('click', function(e) {
		var form = $(this).parents('form'); formReset(form);
	});
	/*[Form reset >]*/
	
	/*[Form valid <]*/
	var formButtonValid = function(form, validator) {
		$(form).find('input, textarea').on('change focus keypress keydown keyup', function(e) {
			if (validator.formValid() > 0) {
				$(form).find('button').addClass('disabled');
			} else {
				$(form).find('button').removeClass('disabled');
			}
		});
	};
	/*[Form valid >]*/
	
	/*[Form fields <]*/
	
	/*[Select <]*/
    $('select').selectOrDie({
		onChange: function() {
			var field = $(this);
			if (field.parents('form').length) {
				field.valid();
			}
		}
	});
	/*[Select >]*/
	
	/*[Radio & checkbox <]*/
	$('input').iCheck({
		radioClass: 'radio',
		checkboxClass: 'checkbox'
	});
	/*[Radio & checkbox >]*/
	
	/*[Form fields >]*/
	
	/*[Form section <]*/
	$('.form-section .display.begin button').click(function(e) {
		var $this = $(this),
			parent = $this.parents('.form-section'),
			diplays = parent.find('.display')
			form = parent.find('.form');
		diplays.hide();
		form.show(0, function(e) {
			form.find('input:first').focus();
			$('html, body').animate({scrollTop: form.offset().top}, 400);
		});
	});
	
	$('.form-section button.close').click(function(e) {
		var $this = $(this),
			parent = $this.parents('.form-section'),
			diplays = parent.find('.display')
			begin = parent.find('.begin');
		diplays.hide();
		begin.show();
	});
	/*[Form section >]*/
	
	/*[Form validation <]*/
	
	/*[Class rules <]*/
	$.validator.addClassRules('_date', {date: true});
	$.validator.addClassRules('_email', {email: true});
	$.validator.addClassRules('_phone', {phone: true});
	$.validator.addClassRules('_digits', {digits: true});
	$.validator.addClassRules('_letters', {letters_only: true});
	$.validator.addClassRules('_letters_digits', {letters_digits: true});
	/*[Class rules >]*/
	
	/*[Callback form <]*/
	if ($('#callback-form').length) {
		var callbackForm = $('#callback-form').validate({
			messages: {
				name: {
					required: 'Пожалуйста, введите Ваше имя',
					letters_only: 'Пожалуйста, вводите только буквы'
				},
				phone: {
					required: 'Пожалуйста, введите Ваш номер телефона',
					digits: 'Пожалуйста, вводите только цифры'
				}
			},
			submitHandler: function() {
				$.ajax({
					type: 'post',
					url: $('#callback-form').attr('action') || '#',
					data: $('#callback-form').serialize(),
					beforeSend: function() {
						TweenMax.to('#preloader', .4, {opacity: 1, display: 'table'});
					},
					success: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							formReset('#order-call-form');
							$('.callback-form .display').hide();
							$('.callback-form .message, .callback-form .message .done').show();
						}});
					},
					error: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							$('.callback-form .display').hide();
							$('.callback-form .message, .callback-form .message .error').show();
						}});
					}
				});
			}
		});
		
		formButtonValid('#callback-form', callbackForm);
	};
	/*[Callback form >]*/
	
	/*[Feedback form <]*/
	if ($('#feedback-form').length) {
		var feedbackForm = $('#feedback-form').validate({
			messages: {
				name: {
					required: 'Пожалуйста, введите Ваше имя',
					letters_only: 'Пожалуйста, вводите только буквы'
				},
				email: {
					email: 'Пожалуйста, введите корректный e-mail',
					required: 'Пожалуйста, введите Ваш e-mail'
				},
				message: {
					required: 'Пожалуйста, введите Ваше сообщение',
					letters_only: 'Пожалуйста, вводите только буквы'
				}
			},
			submitHandler: function() {
				$.ajax({
					type: 'post',
					url: $('#feedback-form').attr('action') || '#',
					data: $('#feedback-form').serialize(),
					beforeSend: function() {
						TweenMax.to('#preloader', .4, {opacity: 1, display: 'table'});
					},
					success: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							formReset('#order-call-form');
							$('.feedback-form .display').hide();
							$('.feedback-form .message, .feedback-form .message .done').show();
						}});
					},
					error: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							$('.feedback-form .display').hide();
							$('.feedback-form .message, .feedback-form .message .error').show();
						}});
					}
				});
			}
		});
		
		formButtonValid('#feedback-form', feedbackForm);
	};
	/*[Feedback form >]*/
	
	/*[Contacts form <]*/
	if ($('#contacts-form').length) {
		var contactsForm = $('#contacts-form').validate({
			messages: {
				name: {
					required: 'Пожалуйста, введите Ваше имя',
					letters_only: 'Пожалуйста, вводите только буквы'
				},
				theme: {
					required: 'Пожалуйста, введите тему сообщения',
					letters_only: 'Пожалуйста, вводите только буквы'
				},
				message: {
					required: 'Пожалуйста, введите Ваше сообщения',
					letters_only: 'Пожалуйста, вводите только буквы'
				}
			},
			submitHandler: function() {
				$.ajax({
					type: 'post',
					url: $('#contacts-form').attr('action') || '#',
					data: $('#contacts-form').serialize(),
					beforeSend: function() {
						TweenMax.to('#preloader', .4, {opacity: 1, display: 'table'});
					},
					success: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							formReset('#contacts-form');
							$('.contacts-form .display').hide();
							$('.contacts-form .message, .contacts-form .message .done').show();
						}});
					},
					error: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							$('.contacts-form .display').hide();
							$('.contacts-form .message, .contacts-form .message .error').show();
						}});
					}
				});
			}
		});
		
		formButtonValid('#contacts-form', contactsForm);
	};
	/*[Contacts form >]*/
	
	/*[Get price form <]*/
	if ($('#get-price-form').length) {
		var getPriceForm = $('#get-price-form').validate({
			messages: {
				name: {
					required: 'Пожалуйста, введите Ваше имя',
					letters_only: 'Пожалуйста, вводите только буквы'
				},
				phone: {
					required: 'Пожалуйста, введите Ваш номер телефона',
					digits: 'Пожалуйста, вводите только цифры'
				}
			},
			submitHandler: function() {
				$.ajax({
					type: 'post',
					url: $('#get-price-form').attr('action') || '#',
					data: $('#get-price-form').serialize(),
					beforeSend: function() {
						TweenMax.to('#preloader', .4, {opacity: 1, display: 'table'});
					},
					success: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							formReset('#get-price-form');
							$('.get-price-block .display').hide();
							$('.get-price-block .message, .get-price-block .message .done').show();
						}});
					},
					error: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							$('.get-price-block .display').hide();
							$('.get-price-block .message, .get-price-block .message .error').show();
						}});
					}
				});
			}
		});
		
		formButtonValid('#get-price-form', getPriceForm);
	};
	/*[Get price form >]*/
	
	/*[Order call form <]*/
	if ($('#order-call-form').length) {
		var orderCallForm = $('#order-call-form').validate({
			messages: {
				name: {
					required: 'Пожалуйста, введите Ваше имя',
					letters_only: 'Пожалуйста, вводите только буквы'
				},
				phone: {
					required: 'Пожалуйста, введите Ваш номер телефона',
					digits: 'Пожалуйста, вводите только цифры'
				}
			},
			submitHandler: function() {
				$.ajax({
					type: 'post',
					url: $('#order-call-form').attr('action') || '#',
					data: $('#order-call-form').serialize(),
					beforeSend: function() {
						TweenMax.to('#preloader', .4, {opacity: 1, display: 'table'});
					},
					success: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							formReset('#order-call-form');
							$('.order-call-block .display').hide();
							$('.order-call-block .message, .order-call-block .message .done').show();
						}});
					},
					error: function() {
						TweenMax.to('#preloader', .4, {opacity: 0, display: 'none', onComplete: function() {
							$('.order-call-block .display').hide();
							$('.order-call-block .message, .order-call-block .message .error').show();
						}});
					}
				});
			}
		});
		
		formButtonValid('#order-call-form', orderCallForm);
	};
	/*[Order call form >]*/
	
	/*[Form validation >]*/
	
	/*[===FORM >]*/
	
	/*[===MAPS <]*/
	if (typeof google != 'undefined') {
		var infoWindow;
		
		/*[Controller <]*/
		var gmap = {
			geocoder: new google.maps.Geocoder(),
			
			/*[=- Show place -=]*/
			showPlace: function(map, zoom, coords, address) {
				if (coords) {
					gmap.showPlaceByCoords(map, zoom, coords);
				} else {
					gmap.showPlaceByAddress(map, zoom, address);
				}
			},
			showPlaceByCoords: function(map, zoom, coords) {
				var coordinates = new google.maps.LatLng(coords[0], coords[1]);
				google.maps.event.trigger(map, 'resize');
				map.setCenter(coordinates);
				map.setZoom(zoom);
			},
			showPlaceByAddress: function(map, zoom, address) {
				gmap.geocoder.geocode({'address': address}, function(results, status) {
					if (results.length) {
						var coordinates = results[0].geometry.location;
						google.maps.event.trigger(map, 'resize');
						map.setCenter(coordinates);
						map.setZoom(zoom);
					}
				});
			},
			/*[=- Show place -=]*/
			
			/*[=- Marker ico -=]*/
			markerSetActivIco: function(marker) {
				var marker_activ_ico = '',
					marker_ico = marker.getIcon();
				
				map_marker_ico.forEach(function(icon_state, icon_key) {
					icon_state.forEach(function(value, key) {
						if (marker_ico == value) {
							marker_activ_ico = map_marker_ico[icon_key][1];
						}
					});
				});
				
				marker.setIcon(marker_activ_ico);
			},
			markerSetDefaultIco: function(markers) {
				$.each(markers, function(index, element) {
					marker = markers[index];
					marker_ico = marker.getIcon();
					
					map_marker_ico.forEach(function(icon_state, icon_key) {
						icon_state.forEach(function(value, key) {
							if (key == 1 && marker_ico == value) {
								marker_ico = map_marker_ico[icon_key][0];
							}
						});
					});
					
					marker.setIcon(marker_ico);
				});
			},
			/*[=- Marker ico -=]*/
			
			/*[=- Create marker -=]*/
			addMarker: function(map, bounds, coords, address, marker_ico, marker_data, marker_array) {
				if (coords) {
					gmap.addMarkerByCoords(map, bounds, coords, marker_ico, marker_data, marker_array);
				} else {
					gmap.addMarkerByAddress(map, bounds, address, marker_ico, marker_data, marker_array);
				};
			},
			addMarkers: function(map, data, marker_array, show_all_markers) {
				bounds = show_all_markers ? new google.maps.LatLngBounds() : false;
				$.each(data, function(id, marker) {
					var coords = marker.coords,
						address = marker.full_address,
						marker_data = marker.description,
						marker_ico = false; //marker.map_marker_ico;
					gmap.addMarker(map, bounds, coords, address, marker_ico, marker_data, marker_array);
				});
			},
			addMarkerByCoords: function(map, bounds, coords, marker_ico, marker_data, marker_array) {
				var coordinates = new google.maps.LatLng(coords[0], coords[1]);
				
				var marker = new google.maps.Marker({
					map: map,
					//icon: marker_ico,
					position: coordinates
				});
				
				if (marker_array) {
					marker_array.push(marker);
				};
				
				if (bounds) {
					bounds.extend(coordinates);
					map.setCenter(bounds.getCenter(), map.fitBounds(bounds));
				} else {
					map.setCenter(coordinates);
				};
				
				gmap.addInfoWindow(map, marker, marker_data, marker_array);
			},
			addMarkerByAddress: function(map, bounds, address, marker_ico, marker_data, marker_array) {
				gmap.geocoder.geocode({'address': address}, function(results, status) {
					if (results.length) {
						var coordinates = results[0].geometry.location;
						
						var marker = new google.maps.Marker({
							map: map,
							//icon: marker_ico,
							position: coordinates
						});
						
						if (marker_array) {
							marker_array.push(marker);
						};
						
						if (bounds) {
							bounds.extend(coordinates);
							map.setCenter(bounds.getCenter(), map.fitBounds(bounds));
						} else {
							map.setCenter(coordinates);
						};
						
						gmap.addInfoWindow(map, marker, marker_data, marker_array);
					}
				});
			},
			/*[=- Create marker -=]*/
			
			/*[=- Create info window -=]*/
			addInfoWindow: function(map, marker, marker_data, marker_array) {
				infoWindow = new google.maps.InfoWindow({
					pixelOffset: new google.maps.Size(0, 0)
				});
				
				google.maps.event.addListener(marker, 'click', function(e) {
					var marker = this;
						
					//gmap.markerSetDefaultIco(marker_array);
					//gmap.markerSetActivIco(marker);
					
					infoWindow.setContent(marker_data);
					infoWindow.open(map, marker);
					
					$('.gm-style-iw').next().addClass('close');
					$('.gm-style-iw').prev().addClass('elements');
					$('.gm-style-iw').parent().addClass('baloon');
					$('.gm-style-iw').prev().children('div:eq(0)').addClass('angle');
					$('.gm-style-iw').prev().children('div:eq(3)').addClass('container');
					$('.gm-style-iw').prev().children('div:eq(1)').addClass('container-wrap');
					$('.gm-style-iw').prev().children('div:eq(2)').addClass('el-3');
				});  
			}
			/*[=- Create info window -=]*/
			
		};
		/*[Controller >]*/
		
		/*[Maps >]*/
		
		/*[Map office <]*/
		var map_office,
			map_office_center,
			map_office_markers = [];
		var initMapOffice = function() {
			if ($('#map-office').length) {
				map_office = new google.maps.Map(document.getElementById('map-office'), {
					zoom: 10,
					zoomControl: true,
					scrollwheel: false,
					mapTypeControl: false,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					zoomControlOptions: {
						style: google.maps.ZoomControlStyle.LARGE
					}
				});
				
				gmap.addMarkers(map_office, map_office_data, map_office_markers, false);
				gmap.showPlace(map_office, 16, map_office_data[1].coords);
				
				google.maps.event.addListener(map_office, 'click', function() {
					infoWindow.close();
				});
			};
		};
		/*[Map office >]*/
		
		/*[Maps >]*/
		
		$(window).load(function() {
			initMapOffice();
		});
	};
	/*[===MAPS >]*/
	
	/*[===IE FIX <]*/
	if ($.browser.msie && $.browser.version <= 8.0) {
		$('.page.home header.main').prepend('<div class="plate-top" />');
		$('.page.inner header.main').prepend('<div class="plate-top" /><div class="plate-bottom" />');
	}
	/*[===IE FIX >]*/
	
});

$(window).load(function() {
	TweenMax.to('#preloader', .6, {opacity: 0, display: 'none'});
});