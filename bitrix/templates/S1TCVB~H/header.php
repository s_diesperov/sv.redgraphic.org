<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
        <?$APPLICATION->ShowMeta("keywords")?>
    	<?$APPLICATION->ShowMeta("description")?>
        
        <title><?$APPLICATION->ShowTitle(false);?></title>
    	 
        <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-[1.11.1]-pack.js" type="text/javascript"></script>

    	<?$APPLICATION->ShowHead();?>
        
        <link rel="SHORTCUT ICON" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/img/elements/favicon.ico"> 
        <link rel="icon" type="image/vnd.microsoft.icon" href="<?=SITE_TEMPLATE_PATH?>/img/elements/favicon.ico">
        
        <link href="<?=SITE_TEMPLATE_PATH?>/css/fonts/fonts.css" rel="stylesheet" type="text/css">
        <link href="<?=SITE_TEMPLATE_PATH?>/css/style-pack.css" rel="stylesheet" type="text/css">
        <link href="<?=SITE_TEMPLATE_PATH?>/css/style.css" rel="stylesheet" type="text/css">

        <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.validate.js" type="text/javascript"></script>
        <script src="<?=SITE_TEMPLATE_PATH?>/js/script-pack.js" type="text/javascript"></script>
        
        <script src="<?=SITE_TEMPLATE_PATH?>/js/site.js" type="text/javascript"></script>
        
        <!--[if IE]>
            <link href="<?=SITE_TEMPLATE_PATH?>/css/ie.css" rel="stylesheet" type="text/css">
            <script src="<?=SITE_TEMPLATE_PATH?>/js/support/html5shiv.js" type="text/javascript"></script>
            <script src="<?=SITE_TEMPLATE_PATH?>/js/support/selectivizr.js" type="text/javascript"></script>
        <![endif]-->
        
    </head>
    
    <body>
    	<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
		
        <?if (CModule::IncludeModule("iblock")):
            $dbEl = CIBlockElement::GetList(Array(), Array("IBLOCK_TYPE"=>"options", "IBLOCK_ID"=>9, "ID"=>35), false, false, array("ID" , "IBLOCK_ID", "HEADER_PHONE", "FOOTER_COPYRIGHT", "FOOTER_SOCIAL_FB", "FOOTER_SOCIAL_IN", "FOOTER_SOCIAL_YT"));

            if($dbEl = $dbEl->GetNextElement())
                $dbPr = $dbEl->GetProperties();
        endif;?>

        <div id="overlay"></div>
        <div id="preloader">
        	<div class="middle">
        		<div class="label">Подождите идет загрузка....</div>
            </div><!-- [ middle ] -->
        </div><!-- [ preloader ] -->
        
        <!-- [> PAGE <] -->
        <div class="page home">
        
        <!-- [> HEADER <] -->
        <header class="main">
        	<div class="section-top">
                <nav class="lang">
                    <div class="trigger"></div>
                	<div class="value">ru</div>
                    <div class="dropdown">
                    	<a class="selected" href="#ru">ru</a>
                    	<a href="#en">en</a>
                    	<a href="#de">de</a>
                    </div><!-- [ dropdown ] -->
                </nav><!-- [ lang ] -->
                
            	<a class="logo" href="index.html">
                	<div class="middle">
                		<img src="<?=SITE_TEMPLATE_PATH?>/img/elements/logo.svg" width="200" height="40" alt="SV Mashinen">
                    </div><!-- [ middle ] -->
                </a><!-- [ logo ] -->
                
                <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                    "ROOT_MENU_TYPE" => "top",  // Тип меню для первого уровня
                    "MENU_CACHE_TYPE" => "A",   // Тип кеширования
                    "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                    "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                    "MENU_CACHE_GET_VARS" => array( // Значимые переменные запроса
                        0 => "",
                    ),
                    "MAX_LEVEL" => "1", // Уровень вложенности меню
                    "CHILD_MENU_TYPE" => "",    // Тип меню для остальных уровней
                    "USE_EXT" => "N",   // Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "DELAY" => "N", // Откладывать выполнение шаблона меню
                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                    ),
                    false
                );?>
                
                <div class="call">
                	<div class="value"><?=$dbPr["HEADER_PHONE"]["VALUE"]?></div>
                	<div class="label">Заказать звонок</div>
                </div><!-- [ call ] -->
                
                <div class="window-contact-forms">
                    <div class="box">
                        <div class="tabs">
                            <nav>
                                <span class="_1 activ"></span>
                                <span class="_2"></span>
                            </nav>
                            <div class="section">
                                <div class="tab activ">
                                    <?$APPLICATION->IncludeComponent("custom:main.feedback", "header_callback", array(
                                        "USE_CAPTCHA" => "N",
                                        "OK_TEXT" => "Спасибо за вашу заявку.",
                                        "EMAIL_TO" => "ald@redgraphic.com",
                                        "REQUIRED_FIELDS" => array(
                                            0 => "NAME",
                                            1 => "PHONE",
                                        ),
                                        "EVENT_MESSAGE_ID" => array(
                                            0 => "7",
                                        )
                                        ),
                                        false
                                    );?>
                                </div><!-- [ tab ] -->
                                
                                <div class="tab">
                                    <?$APPLICATION->IncludeComponent("bitrix:main.feedback", "header_feedback", array(
                                        "USE_CAPTCHA" => "N",
                                        "OK_TEXT" => "Спасибо за вашу заявку.",
                                        "EMAIL_TO" => "ald@redgraphic.com",
                                        "REQUIRED_FIELDS" => array(
                                            0 => "NAME",
                                            1 => "PHONE",
                                        ),
                                        "EVENT_MESSAGE_ID" => array(
                                            0 => "7",
                                        )
                                        ),
                                        false
                                    );?>
                                </div><!-- [ tab ] -->

                            </div><!-- [ section ] -->
                        </div><!-- [ tabs ] -->
                    </div><!-- [ box ] -->
                    <button class="close" type="button">&times;</button>
                </div><!-- [ window-contact-forms ] -->
            </div><!-- [ section-top ] -->
        </header><!-- [ main ] -->
        <!-- [> HEADER <] -->

        <?if($APPLICATION->GetCurPage(false) === '/'):?>
            <!-- [> EXCURSION <] -->
            <div class="excursion">
                <div id="scene">
                    <div class="content">
                        <div class="section">
                            <div class="point _1" data-relation="0:0" style="top:364px;left:630px;"></div>
                            <div class="point _2" data-relation="0:1" style="top:100px;left:955px;"></div>
                        </div><!-- [ section ] -->
                        <div class="section">
                            <div class="point _1" data-relation="1:0" style="top:364px;left:630px;"></div>
                            <div class="point _2" data-relation="1:1" style="top:100px;left:955px;"></div>
                        </div><!-- [ section ] -->
                        <div class="section">
                            <div class="point _1" data-relation="2:0" style="top:364px;left:630px;"></div>
                            <div class="point _2" data-relation="2:1" style="top:100px;left:955px;"></div>
                        </div><!-- [ section ] -->
                    </div><!-- [ content ] -->
                </div><!-- [ scene ] -->
                
                <nav class="tabs">
                    <span class="tab">
                        <div class="label">
                            <span>сельскохозяйственная техника</span>
                        </div><!-- [ label ] -->
                        <button class="close"></button>
                        <div class="dropdown">
                            <div class="title">сельскохозяйственная техника</div>
                            <div class="options">
                                <a class="item" href="#">
                                    <div class="photo">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/images/group-2/1.png" alt="">
                                    </div><!-- [ photo ] -->
                                    <div class="data">
                                        <div class="middle">
                                            <div class="name">организация хранения зерна</div>
                                            <div class="company">ПЧУП «Солигорский комбинат хлебопродуктов»</div>
                                        </div><!-- [ middle ] -->
                                    </div><!-- [ data ] -->
                                </a><!-- [ item ] -->
                                <a class="item" href="#">
                                    <div class="photo">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/images/group-2/2.png" alt="">
                                    </div><!-- [ photo ] -->
                                    <div class="data">
                                        <div class="middle">
                                            <div class="name">оптимизация агроландшафта</div>
                                            <div class="company">ОДО «Восток-Агро-Трейд»</div>
                                        </div><!-- [ middle ] -->
                                    </div><!-- [ data ] -->
                                </a><!-- [ item ] -->
                                <a class="link-to-catalog" href="catalog.html">Перейти к каталогу продукции</a>
                            </div><!-- [ options ] -->
                        </div><!-- [ dropdown ] -->
                    </span>
                    
                    <span class="tab">
                        <div class="label">
                            <span>промышленное оборудование</span>
                        </div><!-- [ label ] -->
                        <button class="close"></button>
                        <div class="dropdown">
                            <div class="title">промышленное оборудование</div>
                            <div class="options">
                                <a class="item" href="#">
                                    <div class="photo">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/images/group-2/1.png" alt="">
                                    </div><!-- [ photo ] -->
                                    <div class="data">
                                        <div class="middle">
                                            <div class="name">организация хранения зерна</div>
                                            <div class="company">ПЧУП «Солигорский комбинат хлебопродуктов»</div>
                                        </div><!-- [ middle ] -->
                                    </div><!-- [ data ] -->
                                </a><!-- [ item ] -->
                                <a class="item" href="#">
                                    <div class="photo">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/images/group-2/2.png" alt="">
                                    </div><!-- [ photo ] -->
                                    <div class="data">
                                        <div class="middle">
                                            <div class="name">оптимизация агроландшафта</div>
                                            <div class="company">ОДО «Восток-Агро-Трейд»</div>
                                        </div><!-- [ middle ] -->
                                    </div><!-- [ data ] -->
                                </a><!-- [ item ] -->
                                <a class="link-to-catalog" href="catalog.html">Перейти к каталогу продукции</a>
                            </div><!-- [ options ] -->
                        </div><!-- [ dropdown ] -->
                    </span>
                    
                    <span class="tab">
                        <div class="label">
                            <span>сервис и запасные части</span>
                        </div><!-- [ label ] -->
                        <button class="close"></button>
                        <div class="dropdown">
                            <div class="title">сервис и запасные части</div>
                            <div class="options">
                                <a class="item" href="#">
                                    <div class="photo">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/images/group-2/1.png" alt="">
                                    </div><!-- [ photo ] -->
                                    <div class="data">
                                        <div class="middle">
                                            <div class="name">организация хранения зерна</div>
                                            <div class="company">ПЧУП «Солигорский комбинат хлебопродуктов»</div>
                                        </div><!-- [ middle ] -->
                                    </div><!-- [ data ] -->
                                </a><!-- [ item ] -->
                                <a class="item" href="#">
                                    <div class="photo">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/img/images/group-2/2.png" alt="">
                                    </div><!-- [ photo ] -->
                                    <div class="data">
                                        <div class="middle">
                                            <div class="name">оптимизация агроландшафта</div>
                                            <div class="company">ОДО «Восток-Агро-Трейд»</div>
                                        </div><!-- [ middle ] -->
                                    </div><!-- [ data ] -->
                                </a><!-- [ item ] -->
                                <a class="link-to-catalog" href="catalog.html">Перейти к каталогу продукции</a>
                            </div><!-- [ options ] -->
                        </div><!-- [ dropdown ] -->
                    </span>
                </nav><!-- [ tabs ] -->
            </div><!-- [ excursion ] -->
            <!-- [> EXCURSION <] -->
        <?endif;?>

        <!-- [> CONTENT <] -->
        <div class="content">

            